import cv2
import numpy as np

#from http://stackoverflow.com/questions/10948589/choosing-correct-hsv-values-for-opencv-thresholding-with-inranges

ORANGE_MIN = np.array([15, 50, 50],np.uint8)
ORANGE_MAX = np.array([45, 255, 255],np.uint8)

# Open video device
cv2.namedWindow( "Original Capture", cv2.WINDOW_AUTOSIZE );
capture1 = cv2.VideoCapture(0)

ret, frame = capture1.read() # Read an frame

while True:
	if frame is not None:   
		cv2.imshow("Original Capture", frame) # Display the frame
		# convert only when not empty
		hsv_img = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
		frame_threshed = cv2.inRange(hsv_img, ORANGE_MIN, ORANGE_MAX)
		cv2.imshow('Color treshold', frame_threshed)


	ret, frame = capture1.read() # Read an frame
	

	if (cv2.waitKey(2) >= 0): # If the user presses a key, exit while loop
		break

cv2.destroyAllWindows() # Close window
cv2.VideoCapture(0).release() # Release video device