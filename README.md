# Project Info

## Context

Basic object detection using python and opencv library

## Color tracking

The color-tracking.py script will perform color tracking. For this a conversion to hue is generated first and then orange is filtered out. 

When picking a color value make sure to double check what range the value should be in. In doubt check this SO question http://stackoverflow.com/questions/10948589/choosing-correct-hsv-values-for-opencv-thresholding-with-inranges

TODO more info
TODO merge color-tracking and color2 scripts

## Shape tracking

shape-tracking.py will perform shape tracking and draw on top of the stream to indicate what shapes (including center points) were found. 

TODO more info

# opencv & python installation

## opencv installation using homebrew

install opencv requires science package

    brew tap homebrew/science
    brew install opencv

## Python

You might need another python installation (if you get sigterm 11 errors). Instructions are from http://www.jeffreythompson.org/blog/2013/08/22/update-installing-opencv-on-mac-mountain-lion/

    brew rem python
    brew install python

And then finish with updating the python path variable (or you'll get module not found errors for cv2 module). This can be done by adding the following line to .bash_profile in your home directory

    vi ~/.bash_profile

and enter (check opencv and python versions)

	export PYTHONPATH=/usr/local/Cellar/opencv/2.4.7.1/lib/python2.7/site-packages:$PYTHONPATH

now activate these changes with

    source ~/.bash_profile
    
# More troubleshooting

I had several errors with example code:

After some more searching I got it working thanks to the following SO question: http://stackoverflow.com/questions/19059459/opencv-wont-capture-from-macbook-pro-isight

The VideoCapture sometimes returns null and that is way the code from the example posted at http://en.wikibooks.org/wiki/Applied_Robotics/Sensors_and_Perception/Open_CV/Basic_OpenCV_Tutorial failed pretty soon. The fixed code looks like this:

non working code (from example)

	#!/usr/bin/python
	import cv2
	 
	# Open video device
	capture1 = cv2.VideoCapture(0)
	while True:
	    ret, img = capture1.read() # Read an image
	    cv2.imshow("ImageWindow", img) # Display the image
	    if (cv2.waitKey(2) >= 0): # If the user presses a key, exit while loop
	        break
	cv2.destroyAllWindows() # Close window
	cv2.VideoCapture(0).release() # Release video device

fixed code

	#!/usr/bin/python
	import cv2
	 
	# Open video device
	cv2.namedWindow( "Display window", 2 );
	capture1 = cv2.VideoCapture(0)
	ret, img = capture1.read() # Read an image

	while True:
	    if img is not None:   
	        cv2.imshow("Display window", img) # Display the image
	    ret, img = capture1.read() # Read an image
	    if (cv2.waitKey(2) >= 0): # If the user presses a key, exit while loop
	        break

	cv2.destroyAllWindows() # Close window
	cv2.VideoCapture(0).release() # Release video device
