#!/usr/bin/python
import cv2
import numpy

# converts frame to hsv for better color matching
def rgb_to_hsv(frame):
	return cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
 
##
# Converts an frame into a binary frame at the specified threshold.
# All pixels with a value <= threshold become 0, while
# pixels > threshold become 1
def do_threshold(frame, threshold = 170):
    (thresh, im_bw) = cv2.threshold(frame, threshold, 255, cv2.THRESH_BINARY)
    return (thresh, im_bw)

def find_contours(frame):
    (contours, hierarchy) = cv2.findContours(frame, mode=cv2.cv.CV_RETR_EXTERNAL, method=cv2.cv.CV_CHAIN_APPROX_SIMPLE)
    return contours

def new_rgb_frame(width, height):
    image = numpy.zeros( (height, width, 3), numpy.uint8)
    return image

def filter_color(frame):
	# get 2 images prepared
	tresholded = new_rgb_frame(frame_threshold.shape[1], frame_threshold.shape[0]);
	tresholded2 = new_rgb_frame(frame_threshold.shape[1], frame_threshold.shape[0]);
	# the colors to match
	# from http://stackoverflow.com/questions/10948589/choosing-correct-hsv-values-for-opencv-thresholding-with-inranges
	ORANGE_MIN = numpy.array([5, 50, 50],numpy.uint8)
	ORANGE_MAX = numpy.array([15, 255, 255],numpy.uint8)
	# as hue wraps around, we need to match it in 2 parts and OR together
	cv2.inRange(frame, ORANGE_MIN, ORANGE_MAX, tresholded);
	# TODO
	# cv2.inRange(hsv_frame, hsv_min2, hsv_max2, thresholded2);
	#return cv2.bitwise_or(thresholded, thresholded2);
	return tresholded;

##
# Finds the centroids of a list of contours returned by
# the find_contours (or cv2.findContours) function.
# If any moment of the contour is 0, the centroid is not computed. Therefore
# the number of centroids returned by this function may be smaller than
# the number of contours passed in.
#
# The return value from this function is a list of (x,y) pairs, where each
# (x,y) pair denotes the center of a contour.
##
def find_centers(contours):
    centers = []
    for contour in contours:
        moments = cv2.moments(contour, True)
 
        # If any moment is 0, discard the entire contour. This is
        # to prevent division by zero.
        if (len(filter(lambda x: x==0, moments.values())) > 0): 
            continue
 
        center = (moments['m10']/moments['m00'] , moments['m01']/moments['m00'])
        # Convert floating point contour center into an integer so that
        # we can display it later.
 
        center = map(lambda x: int(round(x)), center)
        centers.append(center)
    return centers
 
##
# Draws circles on an image from a list of (x,y) tuples
# (like those returned from find_centers()). Circles are
# drawn with a radius of 20 px and a line width of 2 px.
##
def draw_centers(centers, image):
    for center in centers:
        cv2.circle(image, tuple(center), 20, cv2.cv.RGB(0,255,255), 2)
 
# Open video device
cv2.namedWindow( "Original Capture", cv2.WINDOW_AUTOSIZE );
capture1 = cv2.VideoCapture(0)

ret, frame = capture1.read() # Read an frame
if frame is not None:
	frame_hsv = rgb_to_hsv(frame) # Convert frame_orig from video camera from RGB to Grayscale		 
	(thresh, frame_threshold) = do_threshold(frame_hsv, 220)

while True:
	if frame is not None:   
		cv2.imshow("Original Capture", frame) # Display the frame
		# convert only when not empty
		frame_hsv = rgb_to_hsv(frame) # Convert frame_orig from video camera from RGB to Grayscale		 
		(thresh, frame_threshold) = do_threshold(frame_hsv, 220)
		cv2.imshow("HSV Color", frame_hsv)

		frame_filtered = filter_color(frame_hsv);
		cv2.imshow("Filtered Color", frame_filtered);

	ret, frame = capture1.read() # Read an frame
	

	if (cv2.waitKey(2) >= 0): # If the user presses a key, exit while loop
		break

cv2.destroyAllWindows() # Close window
cv2.VideoCapture(0).release() # Release video device







 
 
 
